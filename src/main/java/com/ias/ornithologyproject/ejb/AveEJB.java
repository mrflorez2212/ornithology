/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ias.ornithologyproject.ejb;

import com.ias.ornithologyproject.jpa.Ave;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Marco
 */
@Stateless
@LocalBean
public class AveEJB{
    
    @PersistenceContext(unitName = "BornithologyProjectPU")
    private EntityManager entityManager;
    
    
    /**
     * Metodo para crear o actualizar un ave
     * @param ave 
     */
    public void guardar(Ave ave){
        this.entityManager.merge(ave);
    }    
    
    /**
     * Metodo para eliminar un ave
     * @param ave 
     */
    public void eliminar(Ave ave){
        ave = entityManager.merge(ave);
        this.entityManager.remove(ave);
    }
    
    /**
     * Metodo para obtener todas las aves     
     * @return lista de aves
     */
    public List<Ave> obtenerAves(){
        Query query = this.entityManager.createQuery("SELECT e FROM Ave e");
        return (List<Ave>) query.getResultList();
    }
    
    /**
     * metodo para consultar un ave por codigo
     * @param ave
     * @return 
     */
    public Ave consultarAve(Ave ave){
        return this.entityManager.find(Ave.class, ave.getCdave());
    }
    
    public List<Ave> filtroAve(String nombre,String zona){
        String sql = "SELECT DISTINCT TA.*\n" +
                     "FROM TONT_AVES TA\n" +
             "          LEFT JOIN TONT_AVES_PAIS TAP ON TAP.CDAVE = TA.CDAVE\n" +
             "          LEFT JOIN TONT_PAISES TP ON TP.CDPAIS = TAP.CDPAIS\n" +
             "          LEFT JOIN TONT_ZONAS TZ ON TP.CDZONA = TZ.CDZONA\n"
             + "      WHERE (UPPER(TA.DSNOMBRE) LIKE ?nombre OR UPPER(TA.DSNOMBRE_CIENTIFICO) LIKE ?nombre)\n";
             
            if(zona!=null){
              sql = sql +  "        AND TZ.CDZONA = ?cdzona";
            }   
             
        Query query = this.entityManager.createNativeQuery(sql,Ave.class);
        query.setParameter("nombre", "%"+nombre.toUpperCase()+"%");
        query.setParameter("cdzona", zona);        
        List<Ave> aveList = query.getResultList();
        return aveList;
    }
}
