package com.ias.ornithologyproject.jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Clase para el manejo con la tabla TONT_PAISES
 * @author Marco
 */
@Entity
@Table(name = "TONT_PAISES")
@Data
public class Pais implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id    
    @NotNull    
    @Column(name = "CDPAIS")
    private String cdpais;
    
    @Column(name = "DSNOMBRE")
    @NotNull
    private String dsnombre;
    
    @JoinColumn(name = "CDZONA", referencedColumnName = "CDZONA")
    @ManyToOne(optional = false)
    private Zona cdzona;
    
    @ManyToMany(mappedBy = "paisList",fetch = FetchType.LAZY)
    private List<Ave> aves;

    public Pais() {
    }

    public Pais(String cdpais) {
        this.cdpais = cdpais;
    }

    public Pais(String cdpais, String dsnombre) {
        this.cdpais = cdpais;
        this.dsnombre = dsnombre;
    }
}
