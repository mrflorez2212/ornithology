package com.ias.ornithologyproject.jpa;

import com.ias.ornithologyproject.jpa.Ave;
import com.ias.ornithologyproject.jpa.Zona;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-13T16:26:34")
@StaticMetamodel(Pais.class)
public class Pais_ { 

    public static volatile SingularAttribute<Pais, Zona> cdzona;
    public static volatile SingularAttribute<Pais, String> cdpais;
    public static volatile SingularAttribute<Pais, String> dsnombre;
    public static volatile ListAttribute<Pais, Ave> aves;

}