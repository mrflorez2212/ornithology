/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ias.ornithologyproject.ejb;

import com.ias.ornithologyproject.jpa.Pais;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Marco
 */

@Stateless
@LocalBean
public class PaisEJB {
    
    @PersistenceContext(unitName = "BornithologyProjectPU")
    private EntityManager entityManager;
    
    
    public List<Pais> obtenerPaises(){
        Query query = this.entityManager.createQuery("SELECT e FROM Pais e");
        return (List<Pais>) query.getResultList();
    }
    
    public Pais obtenerPaisxCodigo(String codigo){
        return this.entityManager.find(Pais.class, codigo);
    }
}
