package com.ias.ornithologyproject.jpa;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Clase para el manejo con la tabla TONT_ZONAS
 * @author Marco
 */
@Entity
@Table(name = "TONT_ZONAS")
@Data
public class Zona implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id    
    @Column(name = "CDZONA")
    private String cdzona;
    
    @Column(name = "CDNOMBRE")
    @NotNull
    private String cdnombre;

    public Zona() {
    }

    public Zona(String cdzona) {
        this.cdzona = cdzona;
    }

    public Zona(String cdzona, String cdnombre) {
        this.cdzona = cdzona;
        this.cdnombre = cdnombre;
    }
    
}
