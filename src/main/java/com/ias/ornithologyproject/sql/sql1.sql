/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * creacion de base de datos
 * Author:  Marco
 * Created: 13/06/2018
 */

-- USER SQL
CREATE USER DBORNITHOLOGY IDENTIFIED BY "DBORNITHOLOGY"  ;

-- QUOTAS

-- ROLES
GRANT "DATAPUMP_EXP_FULL_DATABASE" TO DBORNITHOLOGY ;
GRANT "DBA" TO DBORNITHOLOGY ;
GRANT "HS_ADMIN_EXECUTE_ROLE" TO DBORNITHOLOGY ;
GRANT "ADM_PARALLEL_EXECUTE_TASK" TO DBORNITHOLOGY ;
GRANT "CTXAPP" TO DBORNITHOLOGY ;
GRANT "SELECT_CATALOG_ROLE" TO DBORNITHOLOGY ;
GRANT "CONNECT" TO DBORNITHOLOGY ;
GRANT "DATAPUMP_IMP_FULL_DATABASE" TO DBORNITHOLOGY ;
GRANT "OEM_MONITOR" TO DBORNITHOLOGY ;
GRANT "APEX_ADMINISTRATOR_ROLE" TO DBORNITHOLOGY ;
GRANT "AQ_USER_ROLE" TO DBORNITHOLOGY ;
GRANT "SCHEDULER_ADMIN" TO DBORNITHOLOGY ;
GRANT "XDB_SET_INVOKER" TO DBORNITHOLOGY ;
GRANT "RECOVERY_CATALOG_OWNER" TO DBORNITHOLOGY ;
GRANT "DBFS_ROLE" TO DBORNITHOLOGY ;
GRANT "XDB_WEBSERVICES_OVER_HTTP" TO DBORNITHOLOGY ;
GRANT "HS_ADMIN_SELECT_ROLE" TO DBORNITHOLOGY ;
GRANT "PLUSTRACE" TO DBORNITHOLOGY ;
GRANT "RESOURCE" TO DBORNITHOLOGY ;
GRANT "AQ_ADMINISTRATOR_ROLE" TO DBORNITHOLOGY ;
GRANT "DELETE_CATALOG_ROLE" TO DBORNITHOLOGY ;
GRANT "XDB_WEBSERVICES_WITH_PUBLIC" TO DBORNITHOLOGY ;
GRANT "XDB_WEBSERVICES" TO DBORNITHOLOGY ;
GRANT "EXECUTE_CATALOG_ROLE" TO DBORNITHOLOGY ;
GRANT "EXP_FULL_DATABASE" TO DBORNITHOLOGY ;
GRANT "GATHER_SYSTEM_STATISTICS" TO DBORNITHOLOGY ;
GRANT "LOGSTDBY_ADMINISTRATOR" TO DBORNITHOLOGY ;
GRANT "AUTHENTICATEDUSER" TO DBORNITHOLOGY ;
GRANT "OEM_ADVISOR" TO DBORNITHOLOGY ;
GRANT "HS_ADMIN_ROLE" TO DBORNITHOLOGY ;
GRANT "XDBADMIN" TO DBORNITHOLOGY ;
GRANT "IMP_FULL_DATABASE" TO DBORNITHOLOGY ;

-- SYSTEM PRIVILEGES
GRANT CREATE JOB TO DBORNITHOLOGY ;
GRANT DROP ANY CONTEXT TO DBORNITHOLOGY ;
GRANT UPDATE ANY CUBE TO DBORNITHOLOGY ;
GRANT DROP ANY TRIGGER TO DBORNITHOLOGY ;
GRANT MANAGE ANY FILE GROUP TO DBORNITHOLOGY ;
GRANT ALTER PUBLIC DATABASE LINK TO DBORNITHOLOGY ;
GRANT MANAGE FILE GROUP TO DBORNITHOLOGY ;
GRANT ALTER ANY INDEX TO DBORNITHOLOGY ;
GRANT DROP ANY SEQUENCE TO DBORNITHOLOGY ;
GRANT ALTER PROFILE TO DBORNITHOLOGY ;
GRANT UNDER ANY TABLE TO DBORNITHOLOGY ;
GRANT CREATE ASSEMBLY TO DBORNITHOLOGY ;
GRANT DROP ANY LIBRARY TO DBORNITHOLOGY ;
GRANT ALTER ANY EDITION TO DBORNITHOLOGY ;
GRANT CREATE ROLE TO DBORNITHOLOGY ;
GRANT CREATE LIBRARY TO DBORNITHOLOGY ;
GRANT DROP ROLLBACK SEGMENT TO DBORNITHOLOGY ;
GRANT CREATE TRIGGER TO DBORNITHOLOGY ;
GRANT ALTER ANY PROCEDURE TO DBORNITHOLOGY ;
GRANT ADMINISTER DATABASE TRIGGER TO DBORNITHOLOGY ;
GRANT DROP ANY MEASURE FOLDER TO DBORNITHOLOGY ;
GRANT CREATE ANY PROCEDURE TO DBORNITHOLOGY ;
GRANT ALTER ANY OUTLINE TO DBORNITHOLOGY ;
GRANT EXECUTE ANY INDEXTYPE TO DBORNITHOLOGY ;
GRANT CREATE ANY DIRECTORY TO DBORNITHOLOGY ;
GRANT ALTER ANY RULE SET TO DBORNITHOLOGY ;
GRANT ALTER ANY MINING MODEL TO DBORNITHOLOGY ;
GRANT DEBUG CONNECT SESSION TO DBORNITHOLOGY ;
GRANT CREATE ANY MINING MODEL TO DBORNITHOLOGY ;
GRANT ALTER SESSION TO DBORNITHOLOGY ;
GRANT CREATE MATERIALIZED VIEW TO DBORNITHOLOGY ;
GRANT MERGE ANY VIEW TO DBORNITHOLOGY ;
GRANT CREATE ANY INDEX TO DBORNITHOLOGY ;
GRANT CREATE DIMENSION TO DBORNITHOLOGY ;
GRANT EXECUTE ANY RULE SET TO DBORNITHOLOGY ;
GRANT ALTER ANY MATERIALIZED VIEW TO DBORNITHOLOGY ;
GRANT AUDIT SYSTEM TO DBORNITHOLOGY ;
GRANT CREATE OPERATOR TO DBORNITHOLOGY ;
GRANT MANAGE ANY QUEUE TO DBORNITHOLOGY ;
GRANT ALTER ANY SQL PROFILE TO DBORNITHOLOGY ;
GRANT GRANT ANY OBJECT PRIVILEGE TO DBORNITHOLOGY ;
GRANT CREATE INDEXTYPE TO DBORNITHOLOGY ;
GRANT AUDIT ANY TO DBORNITHOLOGY ;
GRANT DEBUG ANY PROCEDURE TO DBORNITHOLOGY ;
GRANT CREATE ANY MEASURE FOLDER TO DBORNITHOLOGY ;
GRANT CREATE ANY SEQUENCE TO DBORNITHOLOGY ;
GRANT CREATE MEASURE FOLDER TO DBORNITHOLOGY ;
GRANT UPDATE ANY CUBE BUILD PROCESS TO DBORNITHOLOGY ;
GRANT CREATE VIEW TO DBORNITHOLOGY ;
GRANT ALTER DATABASE LINK TO DBORNITHOLOGY ;
GRANT ALTER ANY ASSEMBLY TO DBORNITHOLOGY ;
GRANT CREATE ANY EVALUATION CONTEXT TO DBORNITHOLOGY ;
GRANT SELECT ANY MINING MODEL TO DBORNITHOLOGY ;
GRANT DELETE ANY CUBE DIMENSION TO DBORNITHOLOGY ;
GRANT ALTER ANY TABLE TO DBORNITHOLOGY ;
GRANT CREATE SESSION TO DBORNITHOLOGY ;
GRANT CREATE RULE TO DBORNITHOLOGY ;
GRANT BECOME USER TO DBORNITHOLOGY ;
GRANT SELECT ANY TABLE TO DBORNITHOLOGY ;
GRANT INSERT ANY MEASURE FOLDER TO DBORNITHOLOGY ;
GRANT CREATE ANY SQL PROFILE TO DBORNITHOLOGY ;
GRANT FORCE ANY TRANSACTION TO DBORNITHOLOGY ;
GRANT DELETE ANY TABLE TO DBORNITHOLOGY ;
GRANT ALTER ANY SEQUENCE TO DBORNITHOLOGY ;
GRANT SELECT ANY CUBE DIMENSION TO DBORNITHOLOGY ;
GRANT CREATE ANY EDITION TO DBORNITHOLOGY ;
GRANT CREATE EXTERNAL JOB TO DBORNITHOLOGY ;
GRANT DROP ANY MATERIALIZED VIEW TO DBORNITHOLOGY ;
GRANT CREATE ANY CUBE BUILD PROCESS TO DBORNITHOLOGY ;
GRANT FLASHBACK ANY TABLE TO DBORNITHOLOGY ;
GRANT DROP ANY RULE SET TO DBORNITHOLOGY ;
GRANT BACKUP ANY TABLE TO DBORNITHOLOGY ;
GRANT ALTER ANY CUBE TO DBORNITHOLOGY ;
GRANT CREATE TABLE TO DBORNITHOLOGY ;
GRANT EXECUTE ANY LIBRARY TO DBORNITHOLOGY ;
GRANT DROP ANY OUTLINE TO DBORNITHOLOGY ;
GRANT EXECUTE ASSEMBLY TO DBORNITHOLOGY ;
GRANT CREATE ANY DIMENSION TO DBORNITHOLOGY ;
GRANT DROP ANY TABLE TO DBORNITHOLOGY ;
GRANT ALTER ANY CLUSTER TO DBORNITHOLOGY ;
GRANT EXECUTE ANY CLASS TO DBORNITHOLOGY ;
GRANT DROP ANY DIMENSION TO DBORNITHOLOGY ;
GRANT CREATE ANY RULE SET TO DBORNITHOLOGY ;
GRANT SELECT ANY SEQUENCE TO DBORNITHOLOGY ;
GRANT UNDER ANY TYPE TO DBORNITHOLOGY ;
GRANT MANAGE TABLESPACE TO DBORNITHOLOGY ;
GRANT DROP ANY OPERATOR TO DBORNITHOLOGY ;
GRANT CREATE ANY OPERATOR TO DBORNITHOLOGY ;
GRANT EXEMPT IDENTITY POLICY TO DBORNITHOLOGY ;
GRANT CREATE TYPE TO DBORNITHOLOGY ;
GRANT CREATE TABLESPACE TO DBORNITHOLOGY ;
GRANT SELECT ANY TRANSACTION TO DBORNITHOLOGY ;
GRANT DELETE ANY MEASURE FOLDER TO DBORNITHOLOGY ;
GRANT CREATE ANY CUBE TO DBORNITHOLOGY ;
GRANT LOCK ANY TABLE TO DBORNITHOLOGY ;
GRANT CREATE EVALUATION CONTEXT TO DBORNITHOLOGY ;
GRANT DROP ANY TYPE TO DBORNITHOLOGY ;
GRANT ADVISOR TO DBORNITHOLOGY ;
GRANT CREATE PUBLIC DATABASE LINK TO DBORNITHOLOGY ;
GRANT ANALYZE ANY TO DBORNITHOLOGY ;
GRANT DROP ANY RULE TO DBORNITHOLOGY ;
GRANT INSERT ANY CUBE DIMENSION TO DBORNITHOLOGY ;
GRANT CREATE ROLLBACK SEGMENT TO DBORNITHOLOGY ;
GRANT CREATE ANY JOB TO DBORNITHOLOGY ;
GRANT ALTER USER TO DBORNITHOLOGY ;
GRANT QUERY REWRITE TO DBORNITHOLOGY ;
GRANT SELECT ANY DICTIONARY TO DBORNITHOLOGY ;
GRANT CREATE PUBLIC SYNONYM TO DBORNITHOLOGY ;
GRANT GLOBAL QUERY REWRITE TO DBORNITHOLOGY ;
GRANT ALTER ANY CUBE DIMENSION TO DBORNITHOLOGY ;
GRANT CREATE ANY CUBE DIMENSION TO DBORNITHOLOGY ;
GRANT DROP ANY CLUSTER TO DBORNITHOLOGY ;
GRANT CREATE ANY RULE TO DBORNITHOLOGY ;
GRANT UPDATE ANY CUBE DIMENSION TO DBORNITHOLOGY ;
GRANT SYSDBA TO DBORNITHOLOGY ;
GRANT ADMINISTER RESOURCE MANAGER TO DBORNITHOLOGY ;
GRANT CREATE ANY SYNONYM TO DBORNITHOLOGY ;
GRANT DROP ANY SYNONYM TO DBORNITHOLOGY ;
GRANT DROP ANY MINING MODEL TO DBORNITHOLOGY ;
GRANT EXECUTE ANY PROCEDURE TO DBORNITHOLOGY ;
GRANT CREATE SYNONYM TO DBORNITHOLOGY ;
GRANT EXECUTE ANY PROGRAM TO DBORNITHOLOGY ;
GRANT EXECUTE ANY TYPE TO DBORNITHOLOGY ;
GRANT ON COMMIT REFRESH TO DBORNITHOLOGY ;
GRANT CREATE SEQUENCE TO DBORNITHOLOGY ;
GRANT COMMENT ANY MINING MODEL TO DBORNITHOLOGY ;
GRANT ADMINISTER SQL TUNING SET TO DBORNITHOLOGY ;
GRANT SYSOPER TO DBORNITHOLOGY ;
GRANT CREATE ANY INDEXTYPE TO DBORNITHOLOGY ;
GRANT DROP ANY INDEX TO DBORNITHOLOGY ;
GRANT RESTRICTED SESSION TO DBORNITHOLOGY ;
GRANT DEQUEUE ANY QUEUE TO DBORNITHOLOGY ;
GRANT ANALYZE ANY DICTIONARY TO DBORNITHOLOGY ;
GRANT ALTER ANY INDEXTYPE TO DBORNITHOLOGY ;
GRANT ADMINISTER ANY SQL TUNING SET TO DBORNITHOLOGY ;
GRANT CREATE USER TO DBORNITHOLOGY ;
GRANT EXECUTE ANY OPERATOR TO DBORNITHOLOGY ;
GRANT CREATE CUBE BUILD PROCESS TO DBORNITHOLOGY ;
GRANT CREATE PROFILE TO DBORNITHOLOGY ;
GRANT ALTER ANY ROLE TO DBORNITHOLOGY ;
GRANT UPDATE ANY TABLE TO DBORNITHOLOGY ;
GRANT ALTER ANY LIBRARY TO DBORNITHOLOGY ;
GRANT DROP ANY VIEW TO DBORNITHOLOGY ;
GRANT CREATE ANY CLUSTER TO DBORNITHOLOGY ;
GRANT EXECUTE ANY RULE TO DBORNITHOLOGY ;
GRANT ALTER TABLESPACE TO DBORNITHOLOGY ;
GRANT UNDER ANY VIEW TO DBORNITHOLOGY ;
GRANT EXECUTE ANY ASSEMBLY TO DBORNITHOLOGY ;
GRANT GRANT ANY PRIVILEGE TO DBORNITHOLOGY ;
GRANT ALTER ANY TRIGGER TO DBORNITHOLOGY ;
GRANT CREATE ANY VIEW TO DBORNITHOLOGY ;
GRANT EXPORT FULL DATABASE TO DBORNITHOLOGY ;
GRANT ALTER ANY EVALUATION CONTEXT TO DBORNITHOLOGY ;
GRANT FLASHBACK ARCHIVE ADMINISTER TO DBORNITHOLOGY ;
GRANT IMPORT FULL DATABASE TO DBORNITHOLOGY ;
GRANT CREATE ANY OUTLINE TO DBORNITHOLOGY ;
GRANT COMMENT ANY TABLE TO DBORNITHOLOGY ;
GRANT CREATE DATABASE LINK TO DBORNITHOLOGY ;
GRANT DROP PUBLIC SYNONYM TO DBORNITHOLOGY ;
GRANT DROP USER TO DBORNITHOLOGY ;
GRANT CHANGE NOTIFICATION TO DBORNITHOLOGY ;
GRANT CREATE MINING MODEL TO DBORNITHOLOGY ;
GRANT INSERT ANY TABLE TO DBORNITHOLOGY ;
GRANT DROP PROFILE TO DBORNITHOLOGY ;
GRANT CREATE ANY MATERIALIZED VIEW TO DBORNITHOLOGY ;
GRANT CREATE RULE SET TO DBORNITHOLOGY ;
GRANT EXEMPT ACCESS POLICY TO DBORNITHOLOGY ;
GRANT MANAGE SCHEDULER TO DBORNITHOLOGY ;
GRANT READ ANY FILE GROUP TO DBORNITHOLOGY ;
GRANT FORCE TRANSACTION TO DBORNITHOLOGY ;
GRANT DROP ANY CUBE BUILD PROCESS TO DBORNITHOLOGY ;
GRANT ALTER ANY TYPE TO DBORNITHOLOGY ;
GRANT DROP ANY PROCEDURE TO DBORNITHOLOGY ;
GRANT DROP PUBLIC DATABASE LINK TO DBORNITHOLOGY ;
GRANT DROP ANY INDEXTYPE TO DBORNITHOLOGY ;
GRANT DROP ANY SQL PROFILE TO DBORNITHOLOGY ;
GRANT ALTER SYSTEM TO DBORNITHOLOGY ;
GRANT UNLIMITED TABLESPACE TO DBORNITHOLOGY ;
GRANT DROP ANY ROLE TO DBORNITHOLOGY ;
GRANT ALTER ANY DIMENSION TO DBORNITHOLOGY ;
GRANT DROP ANY CUBE DIMENSION TO DBORNITHOLOGY ;
GRANT DROP ANY CUBE TO DBORNITHOLOGY ;
GRANT CREATE ANY TRIGGER TO DBORNITHOLOGY ;
GRANT DROP ANY ASSEMBLY TO DBORNITHOLOGY ;
GRANT CREATE ANY TABLE TO DBORNITHOLOGY ;
GRANT ADMINISTER SQL MANAGEMENT OBJECT TO DBORNITHOLOGY ;
GRANT DROP ANY DIRECTORY TO DBORNITHOLOGY ;
GRANT ENQUEUE ANY QUEUE TO DBORNITHOLOGY ;
GRANT DROP ANY EVALUATION CONTEXT TO DBORNITHOLOGY ;
GRANT CREATE ANY ASSEMBLY TO DBORNITHOLOGY ;
GRANT CREATE ANY TYPE TO DBORNITHOLOGY ;
GRANT CREATE CLUSTER TO DBORNITHOLOGY ;
GRANT CREATE ANY CONTEXT TO DBORNITHOLOGY ;
GRANT EXECUTE ANY EVALUATION CONTEXT TO DBORNITHOLOGY ;
GRANT RESUMABLE TO DBORNITHOLOGY ;
GRANT CREATE ANY LIBRARY TO DBORNITHOLOGY ;
GRANT DROP ANY EDITION TO DBORNITHOLOGY ;
GRANT CREATE PROCEDURE TO DBORNITHOLOGY ;
GRANT ALTER DATABASE TO DBORNITHOLOGY ;
GRANT SELECT ANY CUBE TO DBORNITHOLOGY ;
GRANT GRANT ANY ROLE TO DBORNITHOLOGY ;
GRANT ALTER ANY RULE TO DBORNITHOLOGY ;
GRANT CREATE CUBE DIMENSION TO DBORNITHOLOGY ;
GRANT ALTER ANY OPERATOR TO DBORNITHOLOGY ;
GRANT CREATE CUBE TO DBORNITHOLOGY ;
GRANT ALTER RESOURCE COST TO DBORNITHOLOGY ;
GRANT DROP TABLESPACE TO DBORNITHOLOGY ;
GRANT ALTER ROLLBACK SEGMENT TO DBORNITHOLOGY ;

commit;