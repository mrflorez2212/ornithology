/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ias.ornithologyproject.controller;

import com.ias.ornithologyproject.ejb.*;
import com.ias.ornithologyproject.jpa.*;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import lombok.Data;

/**
 *
 * @author Marco
 */
@ManagedBean
@ViewScoped
@Data
public class AveController {
    
    @EJB
    private AveEJB aveEJB;    
    @EJB
    private PaisEJB paisEJB;
     @EJB
    private ZonaEJB zonaEJB;
    
    
    private List<Ave> listadoAves;
    private Ave ave;
    private List<Pais> listadoPaises;
    private String codigoPaisSeleccionado;
    private List<Zona> listadoZonas;
    private String filtroNombreAve;
    private String codigoZonaSeleccionado;
   
    
    @PostConstruct
    public void init(){
        this.ave = new Ave();
        this.listadoAves = new ArrayList<>();
        this.listadoPaises = this.paisEJB.obtenerPaises();
        this.listadoZonas = this.zonaEJB.obtenerZonas();
        this.ave.setPaisList(new ArrayList<Pais>());        
        consultarAves();
    }
    
    public void guardarAve(){
        FacesContext context = FacesContext.getCurrentInstance();
        try{
            this.aveEJB.guardar(ave);
            consultarAves();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha guardado exitosamente el ave", null));
            this.ave = new Ave();        
            this.ave.setPaisList(new ArrayList<Pais>());        
        }catch(Exception e){
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha ocurrido un error", null)); 
            e.printStackTrace();
        }
    }
    
    public void asociarPais(){
        FacesContext context = FacesContext.getCurrentInstance();
        try{            
            if(this.codigoPaisSeleccionado==null){
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Debe seleccionar un pais",null));                
            }else{
                Pais paisConsultado = paisEJB.obtenerPaisxCodigo(codigoPaisSeleccionado);
                boolean agregar=true;
                for(Pais pais: this.ave.getPaisList()){
                    if(pais.getCdpais().equalsIgnoreCase(paisConsultado.getCdpais())){
                        agregar=false;
                        break;
                    }
                }
                if(agregar)
                    this.ave.getPaisList().add(paisConsultado);
                
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha asociado el pais exitosamente", null));
                
            }
        }catch(Exception e){
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha ocurrido un error", null)); 
            e.printStackTrace();
        }
    }
    
    public void eliminarPais(Pais paisEliminar){
        FacesContext context = FacesContext.getCurrentInstance();
        try{   
           this.ave.getPaisList().remove(paisEliminar);
           context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pais eliminado exiosamente", null));
        }catch(Exception e){
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha ocurrido un error", null)); 
            e.printStackTrace();
        }
    }
    
    private void consultarAves(){
        this.listadoAves = this.aveEJB.obtenerAves();
    }
            
    
    public void cambiarPais(AjaxBehaviorEvent event){        
    }
    
    public void editarAve(Ave ave){
        FacesContext context = FacesContext.getCurrentInstance();
        try{ 
           this.ave = this.aveEJB.consultarAve(ave);           
        }catch(Exception e){
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha ocurrido un error", null)); 
            e.printStackTrace();
        }
    }
    
    public void eliminarAve(Ave ave){
        FacesContext context = FacesContext.getCurrentInstance();
        try{            
           this.aveEJB.eliminar(ave);
           consultarAves();
           context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Ave eliminado exiosamente", null));
        }catch(Exception e){
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha ocurrido un error", null)); 
            e.printStackTrace();
        }
    }
    
    public void filtrarAves(){
        FacesContext context = FacesContext.getCurrentInstance();
        try{
           this.listadoAves = this.aveEJB.filtroAve(this.filtroNombreAve, this.codigoZonaSeleccionado);
        }catch(Exception e){
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha ocurrido un error", null)); 
            e.printStackTrace();
        }
        
    }
    
}
