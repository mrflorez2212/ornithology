package com.ias.ornithologyproject.jpa;

import com.ias.ornithologyproject.jpa.Pais;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-13T16:26:34")
@StaticMetamodel(Ave.class)
public class Ave_ { 

    public static volatile SingularAttribute<Ave, String> dsnombreCientifico;
    public static volatile ListAttribute<Ave, Pais> paisList;
    public static volatile SingularAttribute<Ave, String> cdave;
    public static volatile SingularAttribute<Ave, String> dsnombre;

}