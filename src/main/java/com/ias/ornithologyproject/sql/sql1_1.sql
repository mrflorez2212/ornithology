/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
    Creacion de tablas
 * Author:  Marco
 * Created: 13/06/2018
 */

CREATE TABLE TONT_ZONAS 
(
  CDZONA VARCHAR2(3) NOT NULL 
, CDNOMBRE VARCHAR2(45) NOT NULL 
, CONSTRAINT TONT_ZONAS_PK PRIMARY KEY 
  (
    CDZONA 
  )
  ENABLE 
);

CREATE TABLE TONT_PAISES 
(
  CDPAIS VARCHAR2(3) NOT NULL 
, DSNOMBRE VARCHAR2(100) NOT NULL 
, CDZONA VARCHAR2(3) NOT NULL 
, CONSTRAINT TONT_PAISES_PK PRIMARY KEY 
  (
    CDPAIS 
  )
  ENABLE 
);

ALTER TABLE TONT_PAISES
ADD CONSTRAINT TONT_PAISES_FK1 FOREIGN KEY
(
  CDZONA 
)
REFERENCES TONT_ZONAS
(
  CDZONA 
)
ENABLE;

CREATE TABLE TONT_AVES 
(
  CDAVE VARCHAR2(5) NOT NULL 
, DSNOMBRE VARCHAR2(100) NOT NULL 
, DSNOMBRE_CIENTIFICO VARCHAR2(200) NOT NULL 
, CONSTRAINT TONT_AVES_PK PRIMARY KEY 
  (
    CDAVE 
  )
  ENABLE 
);


CREATE TABLE TONT_AVES_PAIS 
(
  CDPAIS VARCHAR2(3) NOT NULL 
, CDAVE VARCHAR2(5) NOT NULL 
, CONSTRAINT TONT_AVES_PAIS_PK PRIMARY KEY 
  (
    CDPAIS 
  , CDAVE 
  )
  ENABLE 
);

ALTER TABLE TONT_AVES_PAIS
ADD CONSTRAINT TONT_AVES_PAIS_FK1 FOREIGN KEY
(
  CDPAIS 
)
REFERENCES TONT_PAISES
(
  CDPAIS 
)
ENABLE;

ALTER TABLE TONT_AVES_PAIS
ADD CONSTRAINT TONT_AVES_PAIS_FK2 FOREIGN KEY
(
  CDAVE 
)
REFERENCES TONT_AVES
(
  CDAVE 
)
ENABLE;

COMMIT;

