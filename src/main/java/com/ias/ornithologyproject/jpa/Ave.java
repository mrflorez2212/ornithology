/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ias.ornithologyproject.jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Clase para el manejo con la tabla TONT_AVES
 * @author Marco
 */
@Entity
@Table(name = "TONT_AVES")
@Data
public class Ave implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id    
    @NotNull    
    @Column(name = "CDAVE")
    private String cdave;
    
    @Column(name = "DSNOMBRE")
    @NotNull
    private String dsnombre;
    
    @Column(name = "DSNOMBRE_CIENTIFICO")
    @NotNull    
    private String dsnombreCientifico;
    
    @JoinTable(name = "TONT_AVES_PAIS", joinColumns = {        
        @JoinColumn(name = "CDAVE")}, inverseJoinColumns = {@JoinColumn(name = "CDPAIS")})
    @ManyToMany
    private List<Pais> paisList;

    public Ave() {
    }

    public Ave(String cdave) {
        this.cdave = cdave;
    }

    public Ave(String cdave, String dsnombre, String dsnombreCientifico) {
        this.cdave = cdave;
        this.dsnombre = dsnombre;
        this.dsnombreCientifico = dsnombreCientifico;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getCdave() != null ? getCdave().hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {        
        if (!(object instanceof Ave)) {
            return false;
        }
        Ave other = (Ave) object;
        if ((this.getCdave() == null && other.getCdave() != null) || (this.getCdave() != null && !this.getCdave().equals(other.getCdave()))) {
            return false;
        }
        return true;
    }

 @Override
    public String toString() {
        return String.format("Ave, codigo=%s, nombre=%s, nombre_cientifico=%s", this.getCdave(), this.dsnombre, this.getDsnombreCientifico());
    }

    
    
}
